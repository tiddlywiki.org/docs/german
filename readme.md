# Installation


## Install Dependencies

- Install nodejs
- npm Install tiddlywiki

TODO VideoLinks

### Test Basic Functions

```
node --version
tiddlywiki --version
```

## Create a Directory Structure

eg:


```
cd \

New-Item -ItemType "directory" -Path "git\gitlab\tiddlywiki.org"

cd git\gitlab\tiddlywiki.org

mkdir core
mkdir docs
mkdir editions
mkdir i18n
mkdir plugins
mkdir senatus
mkdir themes

```

## Clone the Repository

```
cd /git/gitlab/tiddlywiki.org/docs

git clone https://gitlab.com/tiddlywiki.org/docs/german.git

cd german
```

## Start the Development Server

prerequisites: NODEJS and TiddlyWiki need to be installed. Preferably globally!!

```
npm start
```

Will start a server at [localhost:8080](http://localhost:8080). The output should look like this: 

```
PS D:\git\gitlab\tiddlywiki.org\docs\german> npm start

> at-AT-server@0.0.1 start D:\git\gitlab\tiddlywiki.org\docs\german
> tiddlywiki de-AT-server --server 8080 $:/core/save/all text/plain text/html

Serving on 127.0.0.1:8080
(press ctrl-C to exit)
 syncer-server-filesystem: Dispatching 'save' task: $:/StoryList
```
